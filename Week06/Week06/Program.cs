﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week06
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine(@" Look! Up in the sky! It's a            ");
            Console.WriteLine(@" bird! It's a plane! It's a");
            Console.WriteLine(@" frog! ...A frog???                ,.+----.");
            Console.WriteLine(@"                              ,*""""         I");
            Console.WriteLine(@"             ____           /         Mbp. dP          __");
            Console.WriteLine(@"         ,o@QBBBBBbm,_     ; ,d       /~`QMP'\     _odBBBBbo,");
            Console.WriteLine(@"         OBBROYALBBBBBBBBmOBBBM      |    |   |,*~dBBBBBBBBBBBb ");
            Console.WriteLine(@"         OBBBBBBBBBBqBBBBBBBBBP      |  0 |0 ,'  dBBBBBBBBBBBBBb");
            Console.WriteLine(@"         ""*BBBP""     ^OBBBP""          \__/  ~    OBBBBBBBBBBBBBP");
            Console.WriteLine(@"     /                     :           __.       `0BBBBBBBBBBP|");
            Console.WriteLine(@"                   .-.     `          ~,A.         `0BBBBBBBBP |");
            Console.WriteLine(@"                .-(   `x__  `\        .B*Bb          `""?OOP~   ;");
            Console.WriteLine(@"             .-(   `\/'   `.  `~--++*\{  `Pb           ,'     ,'");
            Console.WriteLine(@"  /         (   `\_/'\__/  )     _    !`-' !`.       ,'     ,'  ");
            Console.WriteLine(@"       /     `\_/'       ,'     L ""+,_|    |-,`.__ ,'    _,','~~\.~~~\");
            Console.WriteLine(@"                \        /\    / ',   ""+.  |/ >   `~~~~~~  ,'    :/~  \");
            Console.WriteLine(@"   /             `~===\/'  `\/'    ""+.   >{  /\        __.,: |   ;    ;");
            Console.WriteLine(@"          /           {         ,|    `\/  \/  `Y~~---~   `. `--:'~~~';");
            Console.WriteLine(@"                       `\     ,&#|    uuu    uuu \         `,   ;^`~~';");
            Console.WriteLine(@"       /               ,d#b,.&###|    UUU.   UUU. `\     __.;~~' `~~~'");
            Console.WriteLine(@"     /              ,d###########|    UUU;   |UU:   Y~~~~");
            Console.WriteLine(@"                ,d############i~      `UUUUUUUUU;   I  ");
            Console.WriteLine(@"          ,d##################I        `UUUUUUU'    I     /   /");
            Console.WriteLine(@"(#############################b.                    Im ");
            Console.WriteLine(@"   )############################b.      \           d#b");
            Console.WriteLine(@"  (###############################b++g+++~#b      ,d##P");
            Console.WriteLine(@"      }####################P~ ~Y####P    J#P`~~~T'");
            Console.WriteLine(@"     (###################P'     `YP'    ;P'   ,'");
            Console.WriteLine(@"       )###############P'        '    ,'    ,'    /");
            Console.WriteLine(@"      (##############P'          __+~'   /##'");
            Console.WriteLine(@" /            Y####P'         ;-'     /###P");
            Console.WriteLine(@"           /   Y#P'         /'      /&##P~");
            Console.WriteLine(@"                |         /'      /&#P~         Not bird, nor plane, nor");
            Console.WriteLine(@"     /          `\______/'\_____/'              even frog. Just little ol'");
            Console.WriteLine(@"                                   /            me Underdog!");
            Console.WriteLine(@"                          /");
            Console.WriteLine(@"             /                          ");
        }
    }
}
